/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (cs110wby): write class javadoc
 *
 * @author cs110wby
 */
public class Kelvin extends Temperature {

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	
	public Kelvin(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		return "" + Float.toString(this.getValue()) + " K";
	}
	@Override
	public Temperature toCelsius() {
		float buffer = this.getValue();
		buffer -= 273;
		this.value = buffer;
		return this;
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		float buffer = this.getValue();
		buffer -= 273;
		buffer *= 1.8;
		buffer += 32;
		this.value = buffer;
		return this;
	}
	@Override
	public Temperature toKelvin(){
		return this;
	}

}
