/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wby
 *
 */
public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + Float.toString(this.getValue()) + " C";
	}
	@Override
	public Temperature toCelsius() {
		
		return this;
		
	}
	@Override
	public Temperature toFahrenheit() {
		float buffer = this.getValue();
		buffer *=9;
		buffer /=5;
		buffer +=32;
		this.value = buffer;
		return this;
	}
	
	public Temperature toKelvin() {
		return null;
	}
}
