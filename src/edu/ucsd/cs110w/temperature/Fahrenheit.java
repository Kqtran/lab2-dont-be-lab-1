/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wby
 *
 */
public class Fahrenheit extends Temperature{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + Float.toString(this.getValue()) + " F";
	}
	@Override
	public Temperature toCelsius() {
		float buffer = this.getValue();
		buffer -= 32;
		buffer *= 5;
		buffer /= 9;
		this.value = buffer;
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	
	public Temperature toKelvin() {
		return null;
	}

}
